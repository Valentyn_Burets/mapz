﻿using System.Drawing;
using System.Windows.Forms;

namespace girl_go_ahead
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.sun = new System.Windows.Forms.PictureBox();
            this.key = new System.Windows.Forms.PictureBox();
            this.coin = new System.Windows.Forms.PictureBox();
            this.player = new System.Windows.Forms.PictureBox();
            this.door = new System.Windows.Forms.PictureBox();
            this.background = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.time_lable = new System.Windows.Forms.Label();
            this.info_lable = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.score_label = new System.Windows.Forms.Label();
            this.monster = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.sun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.key)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.door)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monster)).BeginInit();
            this.SuspendLayout();
            // 
            // gameTimer
            // 
            this.gameTimer.Enabled = true;
            this.gameTimer.Interval = 20;
            this.gameTimer.Tick += new System.EventHandler(this.mainGameTimer);
            // 
            // sun
            // 
            this.sun.Image = ((System.Drawing.Image)(resources.GetObject("sun.Image")));
            this.sun.Location = new System.Drawing.Point(442, 12);
            this.sun.Name = "sun";
            this.sun.Size = new System.Drawing.Size(140, 98);
            this.sun.TabIndex = 15;
            this.sun.TabStop = false;
            // 
            // key
            // 
            this.key.Image = ((System.Drawing.Image)(resources.GetObject("key.Image")));
            this.key.Location = new System.Drawing.Point(876, 401);
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(90, 47);
            this.key.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.key.TabIndex = 14;
            this.key.TabStop = false;
            this.key.Tag = "key";
            // 
            // coin
            // 
            this.coin.Image = ((System.Drawing.Image)(resources.GetObject("coin.Image")));
            this.coin.Location = new System.Drawing.Point(230, 375);
            this.coin.Name = "coin";
            this.coin.Size = new System.Drawing.Size(35, 37);
            this.coin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.coin.TabIndex = 13;
            this.coin.TabStop = false;
            this.coin.Tag = "key";
            // 
            // player
            // 
            this.player.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("player.BackgroundImage")));
            this.player.Image = ((System.Drawing.Image)(resources.GetObject("player.Image")));
            this.player.Location = new System.Drawing.Point(108, 286);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(71, 126);
            this.player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.player.TabIndex = 9;
            this.player.TabStop = false;
            this.player.Tag = "player";
            // 
            // door
            // 
            this.door.Image = global::girl_go_ahead.Properties.Resources.closed_door;
            this.door.Location = new System.Drawing.Point(1373, 334);
            this.door.Name = "door";
            this.door.Size = new System.Drawing.Size(83, 102);
            this.door.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.door.TabIndex = 8;
            this.door.TabStop = false;
            this.door.Tag = "door";
            // 
            // background
            // 
            this.background.Image = ((System.Drawing.Image)(resources.GetObject("background.Image")));
            this.background.Location = new System.Drawing.Point(-2, -3);
            this.background.Name = "background";
            this.background.Size = new System.Drawing.Size(2000, 480);
            this.background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.background.TabIndex = 0;
            this.background.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Time";
            // 
            // time_lable
            // 
            this.time_lable.AutoSize = true;
            this.time_lable.Location = new System.Drawing.Point(53, 9);
            this.time_lable.Name = "time_lable";
            this.time_lable.Size = new System.Drawing.Size(13, 13);
            this.time_lable.TabIndex = 17;
            this.time_lable.Text = "0";
            // 
            // info_lable
            // 
            this.info_lable.AutoSize = true;
            this.info_lable.Location = new System.Drawing.Point(78, 55);
            this.info_lable.Name = "info_lable";
            this.info_lable.Size = new System.Drawing.Size(24, 13);
            this.info_lable.TabIndex = 18;
            this.info_lable.Text = "test";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "current info";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Score";
            // 
            // score_label
            // 
            this.score_label.AutoSize = true;
            this.score_label.Location = new System.Drawing.Point(53, 31);
            this.score_label.Name = "score_label";
            this.score_label.Size = new System.Drawing.Size(35, 13);
            this.score_label.TabIndex = 21;
            this.score_label.Text = "label4";
            // 
            // monster
            // 
            this.monster.Image = ((System.Drawing.Image)(resources.GetObject("monster.Image")));
            this.monster.Location = new System.Drawing.Point(471, 306);
            this.monster.Name = "monster";
            this.monster.Size = new System.Drawing.Size(100, 106);
            this.monster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.monster.TabIndex = 22;
            this.monster.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1065, 480);
            this.Controls.Add(this.monster);
            this.Controls.Add(this.score_label);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.info_lable);
            this.Controls.Add(this.time_lable);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sun);
            this.Controls.Add(this.key);
            this.Controls.Add(this.coin);
            this.Controls.Add(this.player);
            this.Controls.Add(this.door);
            this.Controls.Add(this.background);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyisdown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyisup);
            ((System.ComponentModel.ISupportInitialize)(this.sun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.key)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.door)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monster)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox door;
        private System.Windows.Forms.PictureBox background;
        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.PictureBox coin;
        private System.Windows.Forms.PictureBox key;
        private System.Windows.Forms.PictureBox sun;
        private System.Windows.Forms.Timer gameTimer;
        private Label label1;
        private Label time_lable;
        private Label info_lable;
        private Label label2;
        private Label label3;
        private Label score_label;
        private PictureBox monster;
    }
}

